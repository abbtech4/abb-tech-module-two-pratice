package org.abbtech.lesson1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CalculatorTest {

     Calculator calculator;

     @BeforeEach
    void init(){
         calculator = new Calculator();
     }

    @Test
    @DisplayName("when we passing second parameter as zero it should throw runtime exception which is arithmetic exception")
    void divideTest_ShouldThrowAnErrorWhenSecondParameterIsZero(){
        Assertions.assertThrows(ArithmeticException.class,()->{
            calculator.divide(2.3,0);
        });
    }

    @Test
    @DisplayName("Product of array and integer")
    void multiplyTest_ShouldReturnProductOfIntArray(){
        int[] as= {1,2,3,4};
        int[] actualResult = calculator.multiply(as,2);
        Assertions.assertArrayEquals(new int[]{2, 4, 6, 8},actualResult);

    }

    @Test
    @DisplayName("Product of one negative and zero ")
    void multiplyTest_ShouldNotBeGreaterAndLessThanZero(){
        double actualResult = calculator.multiply(-5,0);
        Assertions.assertFalse(actualResult<0 && actualResult>0);

    }

    @Test
    @DisplayName("Sum of numbers")
    void additionTest_ShouldReturnNullValue(){
         Double actual = calculator.addition(null, 3.2);
         Assertions.assertNull(actual);
    }

    @Test
    @DisplayName("Sum of numbers")
    void additionTest_ShouldNotReturnNullValue(){
        Double actual = calculator.addition(2, 3.2);
        Assertions.assertNotNull(actual);
    }

    @Test
    @DisplayName("Product of zero and one negative ")
    void multiplyTest_ShouldEqualsToZero(){
        double actualResult = calculator.multiply(0,-3);
        Assertions.assertTrue(actualResult==0);
    }

     @Test
     @DisplayName("addition two positive numbers")
    void additionTest_ShouldReturnPositiveNumbers(){
         double actualResult = calculator.addition(5.3,5);
         Assertions.assertEquals(10.3,actualResult);
     }

    @Test
    @DisplayName("Sum of numbers")
    void additionTest_ShouldReturnValues(){
        Double actual = calculator.addition(null, 3.2);
        Assertions.assertSame(null,actual);
    }

     @Test
     @DisplayName("Addition of two negative numbers")
     void additionTest_ShouldReturnSumOfNegativeNumbers(){
         double actual = calculator.addition(-12.4,-2.1);
         Assertions.assertNotSame(-12,actual);
     }

     @Test
     @DisplayName("Addition of one negative and one positive number")
     void additionTest_ShouldReturnSumOfOneNegativeAndOnePositiveNumbers(){
         double actual = calculator.addition(-12.3,3);
         Assertions.assertEquals(-9.3,actual);
     }

    @Test
    @DisplayName("Addition of one positive and one negative number")
    void additionTest_ShouldReturnSumOfOnePositiveAndOneNegativeNumbers(){
        double actual = calculator.addition(12.3,-3);
        Assertions.assertEquals(9.3,actual);
        Assertions.assertNotNull(actual);
    }
    @Test
    @DisplayName("Addition of two zeros")
    void additionTest_ShouldReturnSumOfTwoZeros(){
        double actual = calculator.addition(0,0);
        Assertions.assertEquals(0,actual);
    }

    @Test
    @DisplayName("Addition of one zero and one positive number")
    void additionTest_ShouldReturnSumOfOneZeroAndOnePositiveNumbers(){
        double actual = calculator.addition(0,3);
        Assertions.assertEquals(3,actual);
    }

    @Test
    @DisplayName("Addition of one zero and one negative number")
    void additionTest_ShouldReturnSumOfOneZeroAndOneNegativeNumbers(){
        double actual = calculator.addition(0,-3);
        Assertions.assertEquals(-3,actual);
    }
     @Test
     @DisplayName("Subtraction of two positive numbers")
     void subtractionTest(){
         double actualResult = calculator.subtraction(12,3);
         Assertions.assertEquals(9,actualResult);
     }

     @Test
     @DisplayName("Subtraction of two negative numbers")
     void subtractionTest_ShouldReturnResultOfSubtractionOfTwoNegativeNumbers(){
         double actual = calculator.subtraction(-23.5,-6.5);
         Assertions.assertEquals(-17,actual);
     }

     @Test
     @DisplayName("Subtraction of one negative one positive numbers")
     void subtractionTest_ShouldReturnResultOfSubtractionOfOneNegativeAndOnePositiveNumbers(){
         double actual = calculator.subtraction(-3.3,4.3);
         Assertions.assertEquals(-7.6,actual);
     }

    @Test
    @DisplayName("Subtraction of one positive and one negative numbers")
    void subtractionTest_ShouldReturnResultOfSubtractionOfOnePositiveAndOneNegativeNumbers(){
        double actual = calculator.subtraction(3.3,-4.3);
        Assertions.assertEquals(7.6,actual);
    }

    @Test
    @DisplayName("Subtraction of  zero and one negative numbers")
    void subtractionTest_ShouldReturnResultOfSubtractionOfOneZeroAndOneNegativeNumbers(){
        double actual = calculator.subtraction(0,-4.3);
        Assertions.assertEquals(4.3,actual);
    }

    @Test
    @DisplayName("Subtraction of  zero and  positive number")
    void subtractionTest_ShouldReturnResultOfSubtractionOfOneZeroAndOnePositiveNumbers(){
        double actual = calculator.subtraction(0,4.3);
        Assertions.assertEquals(-4.3,actual);
    }

    @Test
    @DisplayName("Subtraction of one positive and zero number")
    void subtractionTest_ShouldReturnResultOfSubtractionOfOnePositiveAndOneZeroNumbers(){
        double actual = calculator.subtraction(3.3,0);
        Assertions.assertEquals(3.3,actual);
    }

    @Test
    @DisplayName("Subtraction of one negative and zero number")
    void subtractionTest_ShouldReturnResultOfSubtractionOfOneNegativeAndOneZeroNumbers(){
        double actual = calculator.subtraction(-3.3,0);
        Assertions.assertEquals(-3.3,actual);
    }
     @Test
     @DisplayName("Subtraction of two zeros")
     void subtractionTest_ShouldReturnZero(){
         double actual = calculator.subtraction(0,0);
         Assertions.assertEquals(0,actual);
     }


     @Test
     @DisplayName("Product of two positive numbers")
    void multiplyTest(){
         double actualResult = calculator.multiply(5,5);
         Assertions.assertEquals(25,actualResult);
     }

    @Test
    @DisplayName("Product of two negative numbers")
    void multiplyTest_ShouldReturnProductOfTwoNegativeNumbers(){
        double actualResult = calculator.multiply(-5,-5);
        Assertions.assertEquals(25,actualResult);

    }
    @Test
    @DisplayName("Product of one positive and one negative numbers")
    void multiplyTest_ShouldReturnProductOfOnePositiveAndOneNegativeNumbers(){
        double actualResult = calculator.multiply(5,-5);
        Assertions.assertEquals(-25,actualResult);
    }

    @Test
    @DisplayName("Product of one positive and zero ")
    void multiplyTest_ShouldReturnProductOfOnePositiveAndZero(){
        double actualResult = calculator.multiply(5,0);
        Assertions.assertEquals(0,actualResult);
    }






    @Test
     void divideTest(){
         double actual=calculator.divide(10,2);
         Assertions.assertEquals(5,actual);
     }


    @Test
    void divideTest_ShouldReturnZero(){
        double actual=calculator.divide(0,2);
        Assertions.assertEquals(0,actual);
    }
    @Test
    @DisplayName("division of one negative and one positive")
    void divideTest_ShouldReturnNegativeValue(){
        double actual = calculator.divide(-4.2,2);
        Assertions.assertEquals(-2.1,actual);
    }

    @Test
    @DisplayName("division of one positive and one negative")
    void divideTest_ShouldReturnNegativeResult(){
        double actual = calculator.divide(4.2,-2);
        Assertions.assertEquals(-2.1,actual);
    }
    @Test
    @DisplayName("division of two negative numbers")
    void divideTest_ShouldReturnPositiveResult(){
        double actual = calculator.divide(4.2,2);
        Assertions.assertEquals(2.1,actual);
    }

}
