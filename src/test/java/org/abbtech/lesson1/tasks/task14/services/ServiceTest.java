package org.abbtech.lesson1.tasks.task14.services;

import org.abbtech.lesson1.tasks.task14.services.ApplicationService;



import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ServiceTest {
    @Mock
    CalculationService calculationService;
    @InjectMocks
    ApplicationService applicationService;

    @Test
    void multiply_Test(){
        Mockito.when(calculationService.multiply(3,2)).thenReturn(6);
        int actualResult = applicationService.multiply(3,2);
        Assertions.assertEquals(6,actualResult);
    }

    @Test
    void division_Test(){
        Mockito.when(calculationService.division(12,3)).thenReturn(4.0);
        double actual = applicationService.divide(12,3);
        Assertions.assertEquals(4,actual);
    }


    @Test
    void division_Test_ThrowsExceptionWhenDivisionCannotDividedByTwo(){
        Mockito.when(calculationService.division(12,4)).thenReturn(3.0);
        Exception exception = Assertions.assertThrows(ArithmeticException.class,()->applicationService.divide(12,4));
        Assertions.assertInstanceOf(ArithmeticException.class,exception);
    }

    @Test
    void division_Test_ThrowsException(){
        Mockito.when(calculationService.division(12,0)).thenThrow(new ArithmeticException());
        Exception exception = Assertions.assertThrows(ArithmeticException.class,()->applicationService.divide(12,0));
        Assertions.assertInstanceOf(ArithmeticException.class,exception);
    }

    @Test
    void addition_Test(){
        Mockito.when(calculationService.addition(4,3)).thenReturn(7);
        int actual = applicationService.addition(4,3);
        Assertions.assertEquals(7,actual);
    }

    @ParameterizedTest
    @CsvSource(value = {"-4,-3,-7","0,-3,-3"},delimiter = ',')
    void addition_Test_ThrowsException(int a,int b,int result){
        Mockito.when(calculationService.addition(a,b)).thenReturn(result);
        Exception exception = Assertions.assertThrows(ArithmeticException.class,()->applicationService.addition(a,b));
        Assertions.assertInstanceOf(ArithmeticException.class,exception);
    }


    @Test
    void subtract_Test(){
        Mockito.when(calculationService.subtract(4,3)).thenReturn(1);
        int actual = applicationService.subtract(4,3);
        Assertions.assertEquals(1,actual);
    }

}
