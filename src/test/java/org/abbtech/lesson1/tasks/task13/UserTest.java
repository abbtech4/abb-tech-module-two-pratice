package org.abbtech.lesson1.tasks.task13;


import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class UserTest {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserService userService;



    @Test
    void isActiveUser_Test(){

        when(userRepository.findByUsername("ilyas")).thenReturn(new User("ilyas",false));
        Assertions.assertFalse(userService.isUserActive("ilyas"));

    }

    @Test
    void deleteUser_Test() throws Exception {
        when(userRepository.findUserById(12)).thenReturn(new User("ilyas",false));
        userService.deleteUser(12);
        verify(userRepository).findUserById(12);
    }

    @Test
    void deleteUser_TestThrowsException()  {
        when(userRepository.findUserById(12)).thenReturn(null);
       Assertions.assertThrows(Exception.class,()->userService.deleteUser(12));

    }


   @Test
    void getUser_Test() throws Exception {
       when(userRepository.findUserById(12)).thenReturn(new User("ilyas",true));
       User user = userService.getUser(12);
       Assertions.assertSame(userService.getUser(12),user);
   }

   @Test
    void getUser_TestThrowsExceptionWhenCannotFindTheUser() {
        when(userRepository.findUserById(12)).thenReturn(null);

        Assertions.assertThrows(Exception.class,()->userService.getUser(12));
        verify(userRepository).findUserById(12);
    }
}
