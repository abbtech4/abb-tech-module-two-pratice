package org.abbtech.lesson1;

public class Calculator {

    public Double addition(Double a,Double b){
        if (a==null || b== null){
            return null;
        }
        return (a+b);
    }

    public double addition(double a,double b){
        return (a+b);
    }

    public double subtraction(double a,double b){
        return (a-b);
    }

    public double multiply(double a,double b){


        return (a*b);
    }

    public int[] multiply(int[] arr,int a){
      int[] newArr = new int[arr.length];
      for (int i =0;i< arr.length;i++){
          newArr[i]=arr[i]*a;
      }
      return newArr;
    };

    public double divide(double a,double b){
    if (b ==0){
        throw new ArithmeticException();
    }
        return a/b;
    }
}
