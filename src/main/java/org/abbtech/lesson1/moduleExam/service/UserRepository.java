package org.abbtech.lesson1.moduleExam.service;

import org.abbtech.lesson1.tasks.task15.services.CalculationResultDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASS = "pass";

    public void saveUserMember(String name, String email, String password) {
        Connection connection = null;
        String sqlInsert = """
                insert into "public".users(user_name,email,passwrd)
                 values (?,?,?);
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);


            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public User findUser(String username,String password) {
        User user = new User();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sqlSelect = """
                SELECT user_name,email,passwrd FROM "public".users where user_name=? and passwrd=?;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            preparedStatement = connection.prepareStatement(sqlSelect);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setName(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("passwrd"));
            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }
    }
