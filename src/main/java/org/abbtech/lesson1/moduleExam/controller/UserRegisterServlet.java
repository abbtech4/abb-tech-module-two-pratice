package org.abbtech.lesson1.moduleExam.controller;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.lesson1.moduleExam.service.UserRepository;

import java.io.IOException;
import java.io.Writer;

@WebServlet(urlPatterns = {"/user/register"})
public class UserRegisterServlet extends HttpServlet {
UserRepository userRepository;
    @Override
    public void init() throws ServletException {
        userRepository=new UserRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName=req.getParameter("user_name");
        String userPassword=req.getParameter("user_password");
        String userEmail=req.getParameter("user_email");

        userRepository.saveUserMember(userName,userEmail,userPassword);
        Cookie cookie = new Cookie(userName,userPassword);
        cookie.setMaxAge(300);
        resp.addCookie(cookie);

        Writer writer = resp.getWriter();
        writer.write("""
                    {
                            "result":""successfully registered
                                
                          }
                    """);
    }
}
