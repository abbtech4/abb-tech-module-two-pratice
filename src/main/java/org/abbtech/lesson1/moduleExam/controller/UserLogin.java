package org.abbtech.lesson1.moduleExam.controller;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.lesson1.moduleExam.service.User;
import org.abbtech.lesson1.moduleExam.service.UserRepository;

import java.io.IOException;
import java.io.Writer;

@WebServlet(urlPatterns = {"/user/login"})
public class UserLogin extends HttpServlet {
    UserRepository userRepository;

    @Override
    public void init() throws ServletException {
        userRepository=new UserRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user_name=req.getParameter("user_name");
        String user_password=req.getParameter("user_password");

        Writer writer = resp.getWriter();
        User user =userRepository.findUser(user_name,user_password);

        if (user != null) {
            writer.write("""
                {
                    "result":""" + user.getName() + """
               
                }
                """
            );
            Cookie usernameCookie = new Cookie("username", user.getName());
            usernameCookie.setMaxAge(300);
            resp.addCookie(usernameCookie);
        } else {
            writer.write("""
                {
                    "error":"Invalid credentials"
                }
                """);
        }
//        User user =userRepository.userExist(user_name,user_password);
//
//        Writer writer = resp.getWriter();
//        if (user_name.equalsIgnoreCase(user.getName())&&user_password.equals(user_password)){
//            HttpSession httpSession = req.getSession();
//            httpSession.setAttribute("username", user.getName());
//            httpSession.setAttribute("user-role", "profile-role");
//            Cookie cookieUser = new Cookie("username", user.getName());
//            Cookie cookieUserRole = new Cookie("user-role", "profile-role");
//            cookieUser.setMaxAge(300);
//            cookieUserRole.setMaxAge(300);
//            resp.addCookie(cookieUser);
//            resp.addCookie(cookieUserRole);
//            writer.write("User logged in  with username : " + user.getName());
//
//
//        }





    }
}
