package org.abbtech.lesson1.moduleExam.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.lesson1.moduleExam.Auth.SecurityUtil;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "UserLogoutServlet", urlPatterns = {"/user-logout"})
public class UserLogoutServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();

    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie userCookie =
                SecurityUtil.getCookieByName(req.getCookies(), "username");
        Cookie userRoleCookie =
                SecurityUtil.getCookieByName(req.getCookies(), "user-role");
        HttpSession httpSession = req.getSession(false);
        if (Objects.nonNull(userCookie) && Objects.nonNull(userRoleCookie) && Objects.nonNull(httpSession)) {
            userCookie.setValue("");
            userCookie.setMaxAge(0);
            userRoleCookie.setValue("");
            userRoleCookie.setMaxAge(0);
            resp.addCookie(userCookie);
            resp.addCookie(userRoleCookie);
            httpSession.invalidate();
        }
    }
}

