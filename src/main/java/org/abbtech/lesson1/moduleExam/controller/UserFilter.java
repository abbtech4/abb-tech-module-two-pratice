package org.abbtech.lesson1.moduleExam.controller;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.lesson1.moduleExam.Auth.SecurityUtil;

import java.io.IOException;

@WebFilter(urlPatterns = {"/user/task"})
public class UserFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Cookie cookie = SecurityUtil.getCookieByName(req.getCookies(), "username");

        if (cookie != null ) {
           chain.doFilter(request,response);
        } else {
            resp.setStatus(401);
            resp.getWriter().write("You are not authorized user");
        }


    }
}
