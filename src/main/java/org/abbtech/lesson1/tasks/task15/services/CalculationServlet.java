package org.abbtech.lesson1.tasks.task15.services;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Calculation Servlet",
        urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply","/calculator/divide"})
public class CalculationServlet extends HttpServlet {

    private ApplicationService applicationService;
    @Override
    public void init() throws ServletException {
       applicationService = new ApplicationService(new CalculationServiceImpl());
        System.out.println("hello");
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello");
//        String calculationMethod = req.getHeader("x-calculation-method");
       int a = Integer.parseInt(req.getParameter("a"));
       int b = Integer.parseInt(req.getParameter("b"));
        PrintWriter writer = resp.getWriter();


           writer.write("""
                    {
                            "result":""" + applicationService.multiply(a,b) + """
                                
                          }
                    """);


//        if (calculationMethod.equalsIgnoreCase("divide")){
//
//            writer.write("""
//                    {
//                            "result":""" + applicationService.divide(a,b) + """
//
//                          }
//                    """);
//
//        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
