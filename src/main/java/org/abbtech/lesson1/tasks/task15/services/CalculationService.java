package org.abbtech.lesson1.tasks.task15.services;

public interface CalculationService {
  int multiply(int a,int b);
  int subtract(int a,int b);
  int addition(int a,int b);
  double division(double a, double b);
}
