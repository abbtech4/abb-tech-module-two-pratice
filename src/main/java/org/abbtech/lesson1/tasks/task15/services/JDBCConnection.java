package org.abbtech.lesson1.tasks.task15.services;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:6432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASS = "pass";

    public static Connection getConnection() {
        Connection connection;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }
}
