package org.abbtech.lesson1.tasks.task15.services;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class CalculationRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:6432/abb_tech";
    private static final String USER_NAME = "psg_user";
    private static final String PASS = "pass";

    public List<CalculationResultDTO> getCalculationResult(String method) {
        Connection connection = null;
        List<CalculationResultDTO> calculations = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM "CALCULATION".calculation_result where calc_method=?;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            preparedStatement.setString(1, method);
            ResultSet calculationResults = preparedStatement.executeQuery();
            while (calculationResults.next()) {
                int a = calculationResults.getInt("VARIABLE_A");
                int b = calculationResults.getInt("VARIABLE_B");
                int result = calculationResults.getInt("CALC_RESULT");

                calculations.add(new CalculationResultDTO(a, b, result));
            }
            return calculations;

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public void saveCalculationResult(int a, int b, int result,String method) {

        Connection connection = null;
        String sqlInsert = """
                insert into "CALCULATION".calculation_result(variable_a,variable_b,calc_result,calc_method)
                 values (?,?,?,?)
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, result);
            preparedStatement.setString(4, method);

            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public void updateCalculationResult(int id,int variableA,int variableB,int result,String method) {


        Connection connection = null;
        String sqlUpdate = """
                update  "CALCULATION".calculation_result set variable_a = ?,variable_b=?,calc_result=?, calc_method=? where id=? 
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setInt(3, result);
            preparedStatement.setInt(1,variableA);
            preparedStatement.setInt(2,variableB);
            preparedStatement.setInt(5, id);
            preparedStatement.setString(4,method);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

    public void deleteCalculationResult(int id) {

        Connection connection = null;
        String sqlDelete = """
                delete from "CALCULATION".calculation_result where id=?;
                """;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }


}
