package org.abbtech.lesson1.tasks.task15.services;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.lesson1.tasks.task14.services.ApplicationService;

import java.io.IOException;
import java.io.Writer;

public class Crud {

    public void post(HttpServletRequest req, HttpServletResponse resp, String method, ApplicationService applicationService,
                     CalculationRepository calculationRepository) throws IOException {

        if (req.getParameter("x") == null || req.getParameter("y") == null) {

            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("""
                    {
                            "Error":"Parameter 'x' and 'y' are required"
                                
                          }
                    """);
            return;
        }


        Writer writer = resp.getWriter();
        double result;

        try {
            int x = Integer.parseInt(req.getParameter("x"));
            int y = Integer.parseInt(req.getParameter("y"));
            result=applicationService.divide(x,y);
            calculationRepository.saveCalculationResult(x,y,(int)result,"divide");
            writer.write("""
                    {
                            "result":""" + applicationService.divide(x,y) + """
                                
                          }
                    """
            );
        }catch (ArithmeticException e){
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                   {
                                           "exception": "INVALID ARGUMENTS (DIVISION OF THE NUMBERS CAN NOT DIVIDE BY 2
                                           OR CANNOT DIVIDE BY 0)"
                                        }              \s
                                      
                   """);
        }catch (Exception e){
            writer.write("""
                   {
                                           "exception": "something went wrong maybe parameter are not int type?"
                                        }              \s
                                      
                   """);
        }

        resp.setContentType("application/json");
    }

    }

