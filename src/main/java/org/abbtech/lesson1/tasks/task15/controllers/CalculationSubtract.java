package org.abbtech.lesson1.tasks.task15.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.abbtech.lesson1.tasks.task15.services.ApplicationService;
import org.abbtech.lesson1.tasks.task15.services.CalculationRepository;
import org.abbtech.lesson1.tasks.task15.services.CalculationServiceImpl;

import java.io.IOException;
import java.io.Writer;

@WebServlet(urlPatterns = {"/calculation/subtract"})
public class CalculationSubtract extends HttpServlet {
    ApplicationService applicationService;
    CalculationRepository calculationRepository;
    @Override
    public void init() throws ServletException {
        applicationService = new ApplicationService(new CalculationServiceImpl());
        calculationRepository = new CalculationRepository();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id")==null|| req.getParameter("id").isEmpty()){
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("""
                    {
                            "Error":"Parameter 'id' is required"
                                
                          }
                    """);
            return;
        }

        try {
            int id = Integer.parseInt(req.getParameter("id"));
            calculationRepository.deleteCalculationResult(id);
            resp.getWriter().write("""
                    {
                            "Success: successfully deleted id with"""+id+"""
                                
                          }
                    """);
        }catch (Exception e){
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("""
                    {
                            "Error":"id not found or not number format"
                                
                          }
                    """);
        }
        resp.setContentType("application/json");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("x") == null || req.getParameter("y") == null || req.getParameter("id")==null) {

            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("""
                    {
                            "Error":"Parameter 'x' 'result' and 'y' are required"
                                
                          }
                    """);
            return;
        }
        int result;
        Writer writer = resp.getWriter();

        try {
            int x = Integer.parseInt(req.getParameter("x"));
            int y = Integer.parseInt(req.getParameter("y"));
            int id = Integer.parseInt(req.getParameter("id"));
            result=applicationService.subtract(x,y);
            calculationRepository.updateCalculationResult(id,x,y,result,"subtract");
            writer.write("""
                    {
                            "result":""" + "OK" + """
                                
                          }
                    """);


        }catch (ArithmeticException e){
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                   {
                                           "exception": "INVALID ARGUMENTS "
                                        }              \s
                                      
                   """);
        }catch (Exception e){
            writer.write("""
                   {
                                           "exception": "something went wrong maybe parameter are not int type?"
                                        }              \s
                                      
                   """);
        }
        resp.setContentType("application/json");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Writer writer = resp.getWriter();

        try {

            var calculations = calculationRepository.getCalculationResult("subtract");
            writer.write("""
                    {
                            "result":""" + calculations + """
                                
                          }
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS"
                     }                    
                     """);
        }
        resp.setContentType("application/json");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("x") == null || req.getParameter("y") == null) {

            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.getWriter().write("""
                    {
                            "Error":"Parameter 'x' and 'y' are required"
                                
                          }
                    """);
            return;
        }
        int result;

        Writer writer = resp.getWriter();
        try {

            int x = Integer.parseInt(req.getParameter("x"));
            int y = Integer.parseInt(req.getParameter("y"));
            result=applicationService.subtract(x,y);
            calculationRepository.saveCalculationResult(x,y,result,"subtract");

        resp.setStatus(HttpServletResponse.SC_OK);
        writer.write("""
                    {
                            "result":""" + applicationService.subtract(x,y) + """
                                
                          }
                    """);}catch (Exception e){
            writer.write("""
                   {
                                           "exception": "something went wrong maybe parameter are not int type?"
                                        }              \s
                                      
                   """);
        }
        resp.setContentType("application/json");
    }
}
