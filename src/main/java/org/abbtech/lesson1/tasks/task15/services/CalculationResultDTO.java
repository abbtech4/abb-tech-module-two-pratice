package org.abbtech.lesson1.tasks.task15.services;

public record CalculationResultDTO (int a,int b, int result){
}
