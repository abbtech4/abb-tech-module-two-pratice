//package org.abbtech.lesson1.tasks.task14.controllers;
//
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.abbtech.lesson1.tasks.task14.services.ApplicationService;
//import org.abbtech.lesson1.tasks.task14.services.CalculationServiceImpl;
//
//import java.io.IOException;
//import java.io.Writer;
//
//@WebServlet(urlPatterns = {"/calculation/divide"},name = "bolmekchundu")
//public class CalculationDivide extends HttpServlet {
//    ApplicationService applicationService;
//
//    @Override
//    public void init() throws ServletException {
//        applicationService=new ApplicationService(new CalculationServiceImpl());
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        if (req.getParameter("x") == null || req.getParameter("y") == null) {
//
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            resp.getWriter().write("""
//                    {
//                            "Error":"Parameter 'x' and 'y' are required"
//
//                          }
//                    """);
//            return;
//        }
//
//
//        Writer writer = resp.getWriter();
//
//
//        try {
//            int x = Integer.parseInt(req.getParameter("x"));
//            int y = Integer.parseInt(req.getParameter("y"));
//            writer.write("""
//                    {
//                            "result":""" + applicationService.divide(x,y) + """
//
//                          }
//                    """
//            );
//        }catch (ArithmeticException e){
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            writer.write("""
//                   {
//                                           "exception": "INVALID ARGUMENTS (DIVISION OF THE NUMBERS CAN NOT DIVIDE BY 2
//                                           OR CANNOT DIVIDE BY 0)"
//                                        }              \s
//
//                   """);
//        }catch (Exception e){
//            writer.write("""
//                   {
//                                           "exception": "something went wrong maybe parameter are not int type?"
//                                        }              \s
//
//                   """);
//        }
//
//        resp.setContentType("application/json");
//    }
//}
