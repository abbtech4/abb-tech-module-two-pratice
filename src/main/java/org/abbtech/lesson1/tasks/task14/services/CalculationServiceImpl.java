package org.abbtech.lesson1.tasks.task14.services;

public class CalculationServiceImpl implements CalculationService{
    @Override
    public int multiply(int a, int b) {
        return (a*b);
    }

    @Override
    public int subtract(int a, int b) {
        return (a-b);
    }

    @Override
    public int addition(int a, int b) {
        return (a+b);
    }

    @Override
    public double division(double a, double b) {
        return (a/b);
    }
}
