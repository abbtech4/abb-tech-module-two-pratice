package org.abbtech.lesson1.tasks.task13;

public interface UserRepository {
    User findByUsername(String username);


    User findUserById(long userId);
}
