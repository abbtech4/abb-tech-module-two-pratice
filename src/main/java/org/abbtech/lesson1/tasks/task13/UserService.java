package org.abbtech.lesson1.tasks.task13;

public class UserService {
    private final UserRepository userRepository;
    UserService (UserRepository userRepository){
        this.userRepository=userRepository;
    }

    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(long userId) throws Exception {
        User user = userRepository.findUserById(userId);
        if(user==null){
            throw new Exception();
        }
    }

    public User getUser(long userId) throws Exception {
        User user = userRepository.findUserById(userId);
        if(user==null){
            throw new Exception();
        }
        return user;
    }




}
