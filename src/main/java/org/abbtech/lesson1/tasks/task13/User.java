package org.abbtech.lesson1.tasks.task13;

public class User {
    private String name;
    private static int id;
    private boolean isActive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getId() {
        return id;
    }



    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public User(String name, boolean isActive) {
        this.name = name;
        this.isActive = isActive;
        id++;
    }
}
